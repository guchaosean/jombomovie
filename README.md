## Desciption
The movie frontend code challenge for Jombo. React / Redux 


## How to
- Clone the repo
- `npm i && npm start` 
- Go to http://localhost:3000

## Test coverage
- `npm test -- --coverage` (It's not pretty and could be improved)