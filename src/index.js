import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
//import createLogger from 'redux-logger';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import store from './config/store';
import './styles/app.scss';
import App from './jsx/components/App';
import Movies from './jsx/components/pages/movie-list/containers/Movies';
import MovieDetail from './jsx/components/pages/movie-detail/containers/MovieDetail';

const history = syncHistoryWithStore(hashHistory,store);

library.add(
  faArrowLeft,
);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history} >
      <Route path="/" component={App}>
        <IndexRoute component={Movies} />
        <Route path="/movie/:id" component={MovieDetail} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
