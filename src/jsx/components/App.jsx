import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loader from './layout/loader/components/Loader';

const App = (props) => {
  const {
    loading,
  } = props;
  
  return (
    <div>
      <Loader loading={loading}>
        {props.children}
      </Loader>
    </div>
  );
};

App.propTypes = {
  loading: PropTypes.bool,
};

App.defaultProps = {
  loading: false,
};

const mapStateToProps = state => ({
  loading: state.ajaxRequests.calls > 0,
});

export default connect(mapStateToProps)(App);
