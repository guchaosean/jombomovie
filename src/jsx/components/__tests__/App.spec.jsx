import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';
import App from '../App';

describe('AppTest', () => {
  let store = {}, wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <Provider store={store}>
        <App />
      </Provider>,
    );
  });

  test('component rendered successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
