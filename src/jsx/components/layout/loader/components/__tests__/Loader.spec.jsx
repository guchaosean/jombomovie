import React from 'react';
import { shallow } from 'enzyme';
import Loader from '../Loader';

describe('Loader', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <Loader loading>
        <div>children</div>
      </Loader>,
    );
  });

  test('component rendered successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
