import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import { HalfCircleSpinner } from 'react-epic-spinners';
import './Loader.scss';

const Loader = ({ loading, children }) => (
  <React.Fragment>
    <div className={ClassNames('spinner-holder', { 'd-none': !loading })}>
      <HalfCircleSpinner color="#000" />
    </div>
    {children}
  </React.Fragment>
);

Loader.propTypes = {
  loading: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
};

export default Loader;
