import React from 'react'
import { Jumbotron, Container } from 'reactstrap'
import './Header.scss'
import logo from '../../../../images/logo.png';
import { Link } from "react-router";

const Header = () => {
  return (
    <div>
      <Jumbotron fluid className="header">
        <Container fluid className="d-flex justify-content-center">
          <Link to="/" >
            <img className="logo" src={logo} alt="movie" />
          </Link>
        </Container>
      </Jumbotron>
    </div>
  )
};

export default Header;
