import React from 'react';
import { shallow } from 'enzyme';
import Header from '../Header';

describe('MovieDetailTest', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
        <Header />,
    );
  });

  test('component rendered successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
