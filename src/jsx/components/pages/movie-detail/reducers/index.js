import * as constants from '../constants/index';

export default function reducer(state = {
  movieDetails: {},
}, action) {
  switch (action.type) {
    case constants.GET_MOVIE_DETAILS_FULFILLED: {
      return {
        ...state,
        movieDetails: action.payload.data,
      }
    }

    default:
  }

  return state;
}
