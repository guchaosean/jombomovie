import * as constants from '../../constants';
import reducer from '../index';

const initialState = {
  movieDetails: {},
};

describe('Movie Detail Reducer', () => {
  test('default case', () => {
    const action = { type: 'dummy_action' };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('GET_MOVIE_DETAILS_FULFILLED', () => {
    const action = {
      type: constants.GET_MOVIE_DETAILS_FULFILLED,
      payload: {
        data: {
          response: [
            {},
          ],
        },
      },
    };
    expect(reducer(undefined, action)).toMatchSnapshot();
  });
});
