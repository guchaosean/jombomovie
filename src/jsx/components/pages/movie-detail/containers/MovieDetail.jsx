import React, { Component } from 'react'
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { Link } from 'react-router';
import { connect} from "react-redux";
import {Card, CardBody, CardImg, CardText, Row, Col, Container} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import './MovieDetails.scss';
import * as actions from "../actions";
import { URL_IMG, IMG_SIZE_LARGE} from '../../../../constants/index';
import Moment from "moment";

const getUserScore = (number) => {
  return number * 10;
};

const getRunTime = (runtime) => {
  const hour = Math.floor(runtime / 60);
  const min = runtime % 60;
  if (hour !== 0) {
    return (
      `${hour} h ${min} min`
    )
  }
  return (
    `${min} min`
  )
};

class MovieDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movieDetails: {},
    }
  }

  componentDidMount() {
    const {
      getMovieDetails,
    } = this.props.actions;

    getMovieDetails(this.props.params.id)
      .then(() => {
        this.setState({
          movieDetails: this.props.movieDetails,
        }, () => {
          console.log(this.props.movieDetails)
        })
      })
  }

  render() {
    const {
      movieDetails,
    } = this.state;

    const date = Moment(movieDetails.release_date, 'YYYY-MM-DD');
    return (
      <div className="movie-details">
        <Card>
          <Link to="/">
            <FontAwesomeIcon className="faArrowLeft pl-2 pt-4" icon={faArrowLeft} />
          </Link>
          <CardImg top width="100%" src={`${URL_IMG}${IMG_SIZE_LARGE}${movieDetails.backdrop_path}`} alt="movie" />
          <CardBody>
            <Container>
               <Row>
                 <Col sm={3} xs={3}>
                   <img className="poster" src={`${URL_IMG}${IMG_SIZE_LARGE}${movieDetails.poster_path}`} alt="movie" />
                 </Col>
                 <Col sm={9} xs={9} >
                   <Row className="d-flex justify-content-end">
                     <h2>{movieDetails.title}</h2>
                   </Row>
                   <Row className="d-flex justify-content-end info">
                     <p>{`${date.format('YYYY')} • ${getUserScore(movieDetails.vote_average)}% User Score`}</p>
                   </Row>
                   <Row className="d-flex justify-content-end info">
                     <p>{getRunTime(movieDetails.runtime)}</p>
                   </Row>
                 </Col>
               </Row>
              <hr />
              <Row className="content">
                <h1>Overview</h1>
              </Row>
              <Row>
                <CardText>{movieDetails.overview}</CardText>
              </Row>
            </Container>
          </CardBody>
        </Card>
      </div>
    )
  }
}

MovieDetail.propTypes = {
  movieDetails: PropTypes.shape({}).isRequired,
  actions: PropTypes.shape({
    getMovieDetails: PropTypes.func,
  }).isRequired,
};

const mapStateToProps = state => ({
  movieDetails: state.movieDetails.movieDetails,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetail);