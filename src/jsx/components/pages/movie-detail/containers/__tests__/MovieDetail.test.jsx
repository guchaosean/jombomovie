import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';
import MovieDetail from '../MovieDetail';

describe('MovieDetailTest', () => {
  let store, wrapper;
  const props = {
    movieDetails: {
      release_date: '2018-01-01',
      title: 'title',
      overview: 'overview',
      backdrop_path: '',
      poster_path: '',
      vote_average: 5.0,
      runtime: 135,
    },
    actions: {
      getMovieDetails: jest.fn(),
    },
  };

  beforeEach(() => {
    wrapper = shallow(
      <Provider store={store}>
        <MovieDetail {...props} />
      </Provider>,
    );
  });

  test('component rendered successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
