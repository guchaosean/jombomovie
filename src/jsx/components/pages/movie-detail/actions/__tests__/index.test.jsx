import configureStore from 'redux-mock-store';
import promiseMiddleware from 'redux-promise-middleware';
import moxios from 'moxios';
import thunk from 'redux-thunk';
import axios from '../../../../../../config/axiosConfig';
import * as actions from '../index';


const mockStore = configureStore([promiseMiddleware(), thunk]);

describe('movie detail actions', () => {
  beforeEach(() => {
    moxios.install(axios);
  });

  afterEach(() => {
    moxios.uninstall(axios);
  });

  test('getMovieDetails', () => {
    const payload = {
        release_date: '2018-01-01',
        title: 'title',
        overview: 'overview',
        backdrop_path: '',
        poster_path: '',
        vote_average: 5.0,
        runtime: 135,
    };

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: payload,
      });
    });

    const store = mockStore(payload);

    const id = 1;

    return store.dispatch(actions.getMovieDetails(id))
      .then(() => {
        expect(store.getActions()).toMatchSnapshot();
      });
  });
});
