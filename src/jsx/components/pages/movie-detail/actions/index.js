import axios from '../../../../../config/axiosConfig';
import * as constants from '../constants/index';
import { API_KEY } from '../../../../constants/index';

export const getMovieDetails = (id) => ({
  type: constants.GET_MOVIE_DETAILS,
  payload: axios.get(`${constants.API_GET_DETAILS}/${id}${API_KEY}`),
});