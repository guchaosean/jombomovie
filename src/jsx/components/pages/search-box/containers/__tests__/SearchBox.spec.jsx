import React from 'react';
import { shallow } from 'enzyme';
import SearchBox from '../SearchBox';

describe('SearchBoxTest', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <SearchBox />,
    );
  });

  test('component rendered successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

  test('onChange', () => {
    const e = {
      target: {
        value: 'test',
      }
    };
    wrapper.instance().onChange(e,  {'test': 'test'});
    expect(wrapper.state('value')).toEqual('test');
  });

  test('onSuggestionsClearRequested', () => {
    wrapper.instance().onSuggestionsClearRequested();
    expect(wrapper.state('suggestions')).toEqual([]);
  });
});
