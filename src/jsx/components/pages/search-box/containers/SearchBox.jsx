import React, { Component } from 'react'
import Autosuggest from 'react-autosuggest';
import './SearhBox.scss'
import { URL_SEARCH } from '../constants'
import logo from '../../../../../images/logo.png';
import { API_KEY_ALT, URL_IMG, IMG_SIZE_XSMALL } from '../../../../constants'
import { Link } from "react-router";

const getSuggestionValue = movie => movie.title;

const renderSuggestion = movie => (
  <Link to={'/movie/' + movie.id} >
    <img alt={movie.title} className="searchResult-image" src= {movie.img == null ? logo: URL_IMG+IMG_SIZE_XSMALL+movie.img } />
    <span className="movie-list-title">{movie.title} | {movie.year}</span>
  </Link>
);

class SearchBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      suggestions: []
    };
  }

  onChange = (e) => {
    this.setState({
      value: e.target.value,
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength > 0) {
      let url = URL_SEARCH + inputValue + API_KEY_ALT;
      fetch(url)
        .then(response => response.json())
        .then(json => json.results)
        .then(data => {
          const results = data.map(movie => {
            let temp = {};
            temp.id = movie.id;
            temp.title = movie.title;
            temp.img = movie.poster_path;
            temp.year = (movie.release_date === "") ? "0000" : movie.release_date.substring(0,4);
            return temp
          });
          this.setState({
            suggestions: results
          });
        }).catch(error => console.log('Exception to get Suggestions'))
    } else {
      this.setState({
        suggestions: []
      })
    }
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Search',
      value,
      onChange: this.onChange
    };

    // Finally, render it!
    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
      />
    );
  }
}

export default SearchBox;
