export const GET_MOVIES = 'GET_MOVIES';
export const GET_MOVIES_FULFILLED = 'GET_MOVIES_FULFILLED';

export const API_GET_MOVIES = '/discover/movie';
export const API_GET_POPULAR = '&sort_by=popularity.desc';