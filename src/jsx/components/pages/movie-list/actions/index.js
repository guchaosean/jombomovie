import axios from '../../../../../config/axiosConfig';
import * as constants from '../constants/index';
import { API_KEY } from '../../../../constants/index';

export const getMovies = () => ({
  type: constants.GET_MOVIES,
  payload: axios.get(`${constants.API_GET_MOVIES}${API_KEY}${constants.API_GET_POPULAR}`),
});