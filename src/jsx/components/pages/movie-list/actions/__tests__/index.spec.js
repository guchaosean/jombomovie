import configureStore from 'redux-mock-store';
import promiseMiddleware from 'redux-promise-middleware';
import moxios from 'moxios';
import thunk from 'redux-thunk';
import axios from '../../../../../../config/axiosConfig';
import * as actions from '../index';


const mockStore = configureStore([promiseMiddleware(), thunk]);

describe('movies actions', () => {
  beforeEach(() => {
    moxios.install(axios);
  });

  afterEach(() => {
    moxios.uninstall(axios);
  });

  test('getMovies', () => {
    const payload = [
      {
        release_date: '2018-01-01',
        title: 'title',
        overview: 'overview',
        backdrop_path: '',
        poster_path: '',
        vote_average: 5.0,
        runtime: 135,
      },
    ];

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: payload,
      });
    });

    const store = mockStore(payload);

    return store.dispatch(actions.getMovies())
      .then(() => {
        expect(store.getActions()).toMatchSnapshot();
      });
  });
});
