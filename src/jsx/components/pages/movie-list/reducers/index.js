import * as constants from '../constants/index';

export default function reducer(state = {
  movies: [],
}, action) {
  switch (action.type) {
    case constants.GET_MOVIES_FULFILLED: {
      return {
        ...state,
        movies: action.payload.data.results,
      }
    }

    default:
  }

  return state;
}
