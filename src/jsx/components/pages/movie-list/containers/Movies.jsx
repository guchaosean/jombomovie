import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import Moment from 'moment';
import {Card, CardBody, CardTitle, CardSubtitle, CardImg, Col, Container, Row} from 'reactstrap';
import * as actions from '../actions';
import { URL_IMG, IMG_SIZE_LARGE} from '../../../../constants/index';
import './Movies.scss';
import Header from "../../../layout/header/Header";
import SearchBox from "../../search-box/containers/SearchBox";

const roundNumber = (number) => {
   return parseFloat(number).toFixed(1);
};

const getVoteColor = (number) => {
  if (number >= 7) {
    return 'bg-success';
  }
  if ((number < 7) && (number >= 5)) {
    return 'bg-warning';
  }
  return 'bg-danger';
};

const renderMovies = (movies) => {
  let col = [
    [], [], [], []
  ];
  movies.filter((movie) => { return movie.poster_path != null }).map((movie, index) => { // eslint-disable-line
    const date = Moment(movie.release_date, 'YYYY-MM-DD');

    const singleMovie = (
      <Col xs={12} sm={6} md={3} key={movie.id} >
        <Link to={'/movie/' + movie.id} >
          <Card>
            <div className="movie-vote-average pt-2 pl-2">
              <span className={`${getVoteColor(movie.vote_average)} pl-2 pr-2`}>{roundNumber(movie.vote_average)}</span>
            </div>
            <CardImg top width="100%" src={`${URL_IMG}${IMG_SIZE_LARGE}${movie.poster_path}`} alt="Movies" />
            <CardBody>
              <CardTitle>{movie.original_title}</CardTitle>
              <CardSubtitle>{date.format('MMMM YYYY')}</CardSubtitle>
            </CardBody>
          </Card>
        </Link>
      </Col>
    );
    col[index % 4].push(singleMovie)
  });
  return (
    <Row>
      {col[0]}{col[1]}{col[2]}{col[3]}
    </Row>
  )
};

class Movies extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movies: [],
    }
  }


  componentDidMount() {
    const {
      getMovies,
    } = this.props.actions;

    getMovies()
      .then(() => {
        this.setState({
          movies: this.props.movies,
        })
      })
  }

  render() {
    const {
      movies,
    } = this.state;

    return (
      <Container>
        <Header />
        <div className="d-flex justify-content-center">
          <SearchBox />
        </div>
        <h1><b>Popular Movies</b></h1>
        {
          renderMovies(movies)
        }
      </Container>
    );
  }
}

Movies.propTypes = {
  movies: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  actions: PropTypes.shape({
    getMovies: PropTypes.func,
  }).isRequired,
};

const mapStateToProps = state => ({
  movies: state.movies.movies,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
