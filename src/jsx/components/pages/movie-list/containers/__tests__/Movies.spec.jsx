import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';
import Movies from '../Movies';

describe('MoviesTest', () => {
  let store = {}, wrapper;
  const props = {
    movies: [
      {
        release_date: '2018-01-01',
        title: 'title',
        overview: 'overview',
        backdrop_path: '',
        poster_path: '',
        vote_average: 5.0,
        runtime: 135,
      },
    ],
    actions: {
      getMovies: jest.fn(),
    },
  };

  beforeEach(() => {
    wrapper = shallow(
      <Provider store={store}>
        <Movies {...props} />
      </Provider>,
    );
  });

  test('component rendered successfully', () => {
    expect(wrapper).toMatchSnapshot();
  });

});
