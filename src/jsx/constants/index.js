export const API_BASE = 'https://api.themoviedb.org/3';

export const URL_IMG = 'https://image.tmdb.org/t/p/';
export const API_KEY = '?api_key=6ed12e064b90ae1290fa326ce9e790ff';
export const API_KEY_ALT = '&api_key=6ed12e064b90ae1290fa326ce9e790ff';
export const IMG_SIZE_LARGE = 'w342/';
export const IMG_SIZE_XSMALL = 'w45/';