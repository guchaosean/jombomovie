import {combineReducers} from 'redux'
import { routerReducer } from 'react-router-redux'
import movies from '../components/pages/movie-list/reducers'
import movieDetails from '../components/pages/movie-detail/reducers'
import ajaxRequests from '../components/layout/loader/reducers';

export default combineReducers({
  movies,
  ajaxRequests,
  movieDetails,
  routing: routerReducer,
})
