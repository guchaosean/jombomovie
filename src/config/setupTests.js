import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

// resolve any 'Error: Not implemented' errors reported by jsdom when running tests
window.scrollTo = () => {};
