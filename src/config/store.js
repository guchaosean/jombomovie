import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import errorHandlingMiddleware from './errorHandlingMiddleware';
import reducer from '../jsx/reducers/index';

// only add dev tools or logging in development or debug mode
let devTools = {};

const middleware = applyMiddleware(
  errorHandlingMiddleware,
  promise(),
  thunk,
  logger({
    collapsed: true,
  }),
);

devTools = composeWithDevTools();


export default createStore(reducer, devTools, middleware);
