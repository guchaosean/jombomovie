import axios from 'axios';
import { API_BASE } from '../jsx/constants';

// configure base url
const instance = axios.create({
  baseURL: API_BASE,
});

export default instance;
