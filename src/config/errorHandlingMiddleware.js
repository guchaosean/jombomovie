import isPromise from 'is-promise';

const errorHandlingMiddleware = () => next => (action) => {
  // If not a promise, continue on
  if (!isPromise(action.payload)) {
    return next(action);
  }

  /*
   * Another solution would would be to include a property in `meta`
   * and evaulate that property.
   *
   * if (action.meta.globalError === true) {
   *   // handle error
   * }
   *
   * The error middleware serves to dispatch the initial pending promise to
   * the promise middleware, but adds a `catch`.
   */
  let {
    type,
  } = action;
  type = String(type);

  if (type.substring(type.length - 9) === '_REJECTED') {
    // Dispatch initial pending promise, but catch any errors
    return next(action).catch((error) => {
      // eslint-disable-next-line no-console
      console.error(error);

      return error;
    });
  }

  return next(action);
};

export default errorHandlingMiddleware;
